openParenthises = ["[","{","("] 
closeParenthises = ["]","}",")"]

def isBalanced(s): 
    stack = [] 
    for i in s: 
        if i in openParenthises: 
            stack.append(i)
        # IF current current character is not opening 
        # bracket, then it must be closing. So stack 
        # cannot be empty at this point.
        elif (len(stack) == 0):
            return "False"
        elif i in closeParenthises: 
            position = closeParenthises.index(i) 
            if ((len(stack) > 0) and
                (openParenthises[position] == stack[len(stack)-1])): 
                stack.pop() 
            else: 
                return "False"
    if len(stack) == 0: 
        return "True"
    else:
        return "False"