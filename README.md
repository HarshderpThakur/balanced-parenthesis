Steps:
1. Run the file and call the function isBalanced(string), which takes one parameter of string type.
2. It will return true if the give string has balanced paranthesis. Otherwise you will get False as output.
For example:
Input    Output
()[]{}       True
([{}])       True
([]{})       True
([)]         False
([]          False
[])          False